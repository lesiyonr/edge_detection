#!/bin/bash

echo "" > time.txt 
for n in {1..100};
do
	echo $n
	{ time $1 ; } 2>> time.txt
done
avtime=`cat time.txt | grep real | cut -d "m" -f 2 | cut -d "s" -f1 | jq -s add/length`

echo ""
echo ""
echo "The average time after 100 runs is $avtime"

