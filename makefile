all: process process_loops process_parallel 

main_process.o: main_process.c
	gcc -O3 -c main_process.c 
main_process_rearrange_loops.o: main_process_rearrange_loops.c
	gcc -O3 -c main_process_rearrange_loops.c
main_process_parallel.o: main_process_parallel.c
	gcc -O3 -fopenmp -c main_process_parallel.c
png_util.o: png_util.c
	gcc -O3 -l lpng16 -c png_util.c
process: main_process.o png_util.o
	gcc -O3 -o process -lm -l png16 main_process.o png_util.o
process_loops: main_process_rearrange_loops.o png_util.o
	gcc -O3 -o process_loops -lm -l png16 main_process_rearrange_loops.o png_util.o
process_parallel: main_process_parallel.o png_util.o
	gcc -O3 -fopenmp -o process_parallel -lm -l png16 main_process_parallel.o png_util.o

test: test0 test1 test2

test0: process 
	./process ./images/cube.png test.png
	./process ./images/earth.png earth.png
	./process ./images/MSUStadium.png MSUStadium.png
	 ./process ./images/sparty.png sparty.png
test1: process_loops
	./process_loops ./images/earth.png earth.png
	./process_loops ./images/cube.png test.png
	./process_loops ./images/MSUStadium.png MSUStadium.png
	./process_loops ./images/sparty.png sparty.png
	
test2: process_parallel
	./process_parallel ./images/earth.png earth.png
	./process_parallel ./images/cube.png test.png
	./process_parallel ./images/MSUStadium.png MSUStadium.png
	./process_parallel ./images/sparty.png sparty.png

clean:
	rm *.o
	rm *.png
	rm process
	rm process_loops
	rm process_parallel
	clear 
